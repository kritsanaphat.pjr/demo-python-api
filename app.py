from flask import Flask, request
from bson import ObjectId
from pymongo.mongo_client import MongoClient
  
app = Flask(__name__)
  
uri = "mongodb+srv://dip:1234@dip-uat.fvxcp8m.mongodb.net/?retryWrites=true&w=majority"
cluster = MongoClient(uri)
db      = cluster['dip-demo-api']
col     = db['account']
  

try:
    cluster.admin.command('ping')
    print("Pinged your deployment. You successfully connected to MongoDB!")
except Exception as e:
    print(e)
  
@app.route('/')
def index():
  
    return "Hello"

@app.route("/get", methods=['GET'])
def get_one_movie():
    account = col.find({})
    i = 0
    listAccount = []
    for document in account:
        account_dict = {}
        object_id_str = str(document['_id'])
        account_dict["id"] = object_id_str
        account_dict["username"] = document['username']
        account_dict["password"] = document['password']
        listAccount.append(account_dict)
    
    print(listAccount)
    return {"data":listAccount}


@app.route("/getByID", methods=['GET'])
def get_by_id():

    id = request.json['id']
    str_id_objeect = ObjectId(id)
    print(type(str_id_objeect))
    account = col.find({"_id":str_id_objeect})
    listAccount = []
    for document in account:
        account_dict = {}
        object_id_str = str(document['_id'])
        account_dict["id"] = object_id_str
        account_dict["username"] = document['username']
        account_dict["password"] = document['password']
        print("0",account_dict)
        listAccount.append(account_dict)
            
    return {"data":listAccount}


@app.route('/create', methods=['POST'])
def create():


    username = request.json['username']
    password = request.json['password']
    
    account_dict = {
         "username"   : username,
         "password"   : password
     }
    
    col.insert_one(account_dict)
    return {"message":"success"}

@app.route("/update", methods=['POST'])
def edit_password():

    username  = request.json['username']
    password = request.json['password']
    account_dict = {
         "username" : username,
         "password": password
    }
    col.update_many({'username': username}, {'$set': account_dict})
    return {"message":"success"}

@app.route('/delete', methods=['DELETE'])
def delete_one_movie():
    
        
    id = request.json['id']
    str_id_objeect = ObjectId(id)
    print(str_id_objeect)
    col.delete_many({'_id': str_id_objeect})
    return {"message":"success"}


if __name__ == '__main__':
    app.run(debug=True,port=4200)